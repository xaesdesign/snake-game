use piston_window::*;
use piston_window::types::Color;

use rand::{ thread_rng, Rng };

use crate::snake::{ Direction, Snake };
use crate::draw::{ draw_square, draw_rectangle };

const FOOD_COLOR: Color = [0.80, 0.00, 0.00, 1.0];
const BORDER_COLOR: Color = [0.00, 0.00, 0.00, 1.0];
const GAMEOVER_COLOR: Color = [0.90, 0.00, 0.00, 0.5];

const MOVING_PERIOD: f64 = 0.15;
const RESTART_TIME: f64 = 1.0;

pub struct Game {
    snake: Snake,
    width: i32,
    height: i32,
    food_x: i32,
    food_y: i32,
    food_exists: bool,
    game_over: bool,
    waiting_time: f64
}

impl Game {

    pub fn new(width: i32, height: i32) -> Game {
        Game {
            snake: Snake::new(5, 5),
            waiting_time: 0.0,
            food_exists: true,
            food_x: 10,
            food_y: 10,
            width,
            height,
            game_over: false
        }
    }

    pub fn key_pressed(&mut self, key: Key) {

        if self.game_over { return; }

        let new_direction = match key {
            Key::Up => Direction::Up,
            Key::Down => Direction::Down,
            Key::Left => Direction::Left,
            Key::Right => Direction::Right,
            _ => { self.snake.head_direction() }
        };

        // If the key pressed is the opposite of the snake's direction: do nothing.

        if new_direction == self.snake.head_direction().opposite() {
            return;
        }

        self.update_snake(Some(new_direction));

    }

    pub fn draw(&self, context: &Context, buffer: &mut G2d) {

        self.snake.draw(context, buffer);

        if self.food_exists {
            draw_square(FOOD_COLOR, self.food_x, self.food_y, context, buffer);
        }

        draw_rectangle(BORDER_COLOR, 0, 0, self.width, 1, context, buffer);
        draw_rectangle(BORDER_COLOR, 0, self.height - 1, self.width, 1, context, buffer);
        draw_rectangle(BORDER_COLOR, 0, 0, 1, self.height, context, buffer);
        draw_rectangle(BORDER_COLOR, self.width - 1, 0, 1, self.height, context, buffer);

        if self.game_over {
            draw_rectangle(GAMEOVER_COLOR, 0, 0, self.width, self.height, context, buffer);
        }

    }

    pub fn update(&mut self, delta_time: f64) {

        self.waiting_time += delta_time;

        if self.game_over {
            if self.waiting_time > RESTART_TIME { self.restart(); }
            return;
        }

        if !self.food_exists { self.add_food(); }
        if self.waiting_time > MOVING_PERIOD {  self.update_snake(None); }

    }

    pub fn add_food(&mut self) {

        // Generating random values for new food position.

        let (mut food_x, mut food_y) = self.random_position();

        while self.snake.overlaps_tail(food_x, food_y) {
            let position = self.random_position();
            food_x = position.0;
            food_y = position.1;
        }

        self.food_x = food_x;
        self.food_y = food_y;
        self.food_exists = true;

    }

    fn random_position(&self) -> (i32, i32) {
        let mut rng = thread_rng();
        (rng.gen_range(1..self.width - 1), rng.gen_range(1..self.height - 1))
    }

    fn check_snake_alive(&self, direction: Option<Direction>) -> bool {

        let next_block = self.snake.next_head(direction);
        let (next_x, next_y) = (next_block.x, next_block.y);

        if self.snake.overlaps_tail(next_x, next_y) {
            return false
        }

        next_x > 0 && next_y > 0 && next_x < self.width - 1 && next_y < self.height - 1

    }

    fn check_eating(&mut self) {
        let (head_x, head_y) = self.snake.head_position();
        if self.food_exists && head_x == self.food_x && head_y == self.food_y {
            self.food_exists = false;
            self.snake.restore_tail();
        }
    }

    fn update_snake(&mut self, direction: Option<Direction>) {

        if self.check_snake_alive(direction) {
            self.snake.move_forward(direction);
            self.check_eating();
        } else {
            self.game_over = true;
        }

        self.waiting_time = 0.0;

    }

    fn restart(&mut self) {
        self.snake = Snake::new(5, 5);
        self.waiting_time = 0.0;
        self.game_over = false;
        self.add_food();
    }

}