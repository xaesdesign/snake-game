use std::collections::LinkedList;
use piston_window::{ Context, G2d };
use piston_window::types::Color;

use crate::draw::{ draw_square };

const SNAKE_INITIAL_SIZE: u32 = 3;
const SNAKE_COLOR: Color = [0.00, 0.80, 0.00, 1.0];

// Direction Definitions.

#[derive(Copy, Clone, PartialEq)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right
}

impl Direction {

    pub fn opposite(&self) -> Direction {
        match self {
            Direction::Up => Direction::Down,
            Direction::Down => Direction::Up,
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left
        }
    }

    pub fn next(&self, x: i32, y: i32) -> Block {
        match *self {
            Direction::Up => Block { x, y: y - 1 },
            Direction::Down => Block { x, y: y + 1 },
            Direction::Left => Block { x: x - 1, y },
            Direction::Right => Block { x: x + 1, y }
        }
    }

}

// Block Definitions.

#[derive(Debug, Clone)]
pub struct Block {
    pub(crate) x: i32,
    pub(crate) y: i32
}

// Snake Definitions.

pub struct Snake {
    direction: Direction,
    body: LinkedList<Block>,
    tail: Option<Block>
}

impl Snake {

    pub fn new(x: i32, y: i32) -> Snake {

        let mut body: LinkedList<Block> = LinkedList::new();

        // Creating initials blocks of snake.

        for i in 0..SNAKE_INITIAL_SIZE {
            body.push_back(Block{ x: x + ((SNAKE_INITIAL_SIZE - i) as i32), y })
        }

        Snake {
            direction: Direction::Right,
            body,
            tail: None
        }

    }

    pub fn draw(&self, context: &Context, buffer: &mut G2d) {
        for block in &self.body {
            draw_square(SNAKE_COLOR, block.x, block.y, context, buffer);
        }
    }

    pub fn head_position(&self) -> (i32, i32) {
        let head_block = &self.body.front().unwrap();
        (head_block.x, head_block.y)
    }

    pub fn move_forward(&mut self, direction: Option<Direction>) {

        if let Some(d) = direction { self.direction = d }

        let (last_x, last_y) = self.head_position();

        // Creating the new block towards the snake direction.

        let new_block = self.direction.next(last_x, last_y);

        // Pushing the new block onto the body.

        self.body.push_front(new_block);

        // Removing the last block (tail) and putting it as snake's optional tail.

        let last_block = self.body.pop_back().unwrap();
        self.tail = Some(last_block);

    }

    pub fn head_direction(&self) -> Direction {
        self.direction
    }

    pub fn next_head(&self, direction: Option<Direction>) -> Block {

        let (head_x, head_y) = self.head_position();

        let move_direction = match direction {
            None => self.head_direction(),
            Some(d) => d
        };

        move_direction.next(head_x, head_y)

    }

    pub fn restore_tail(&mut self) {
        self.body.push_back(self.tail.clone().unwrap());
    }

    pub fn overlaps_tail(&self, x: i32, y: i32) -> bool {
        self.body.iter().any(|block| {
            block.x == x && block.y == y
        })
    }

}