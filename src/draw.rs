use piston_window::{ rectangle, Context, G2d };
use piston_window::types::{ Color };

const BLOCK_SIZE: f64 = 25.0;

pub fn to_coord(coordinate: i32) -> f64 { BLOCK_SIZE * (coordinate as f64) }
pub fn to_coord_u32(coordinate: i32) -> u32 { to_coord(coordinate) as u32 }

pub fn draw_square(color: Color, x: i32, y: i32, context: &Context, buffer: &mut G2d) {

    let gui_x = to_coord(x);
    let gui_y = to_coord(y);

    rectangle(color, [gui_x, gui_y, BLOCK_SIZE, BLOCK_SIZE], context.transform, buffer)

}

pub fn draw_rectangle(
    color: Color,
    x: i32,
    y: i32,
    width: i32,
    height: i32,
    context: &Context,
    buffer: &mut G2d
){

    let gui_x = to_coord(x);
    let gui_y = to_coord(y);
    let width = (width as f64) * BLOCK_SIZE;
    let height = (height as f64) * BLOCK_SIZE;

    rectangle(color, [gui_x, gui_y, width, height], context.transform, buffer);

}